   /* <div class="fullScreen"> <img src="./images/wrong.png"> <button class="close">Close</button> </div> */
document.addEventListener('DOMContentLoaded', function() {
    var ul = document.querySelector('.gallery');
    var lis = ul.querySelectorAll('li');
    var imgs = ul.querySelectorAll('img');
    
    for (var i = 0; i < imgs.length; i++) {
        imgs[i].addEventListener('click', onClickImgsHandler);
    }

    
})

function onClickImgsHandler(event) {
    console.log(this.src);
    createElement(this.src);
}

function createElement(url) {
    var div = document.createElement('div');
    div.classList.add('fullScreen');
    var img = document.createElement('img');

    if (!url) {
        img.src = "./images/wrong.png";
    } else {
        img.src = url;
    }
    var button = document.createElement('button');
    button.classList.add('close');
    button.innerHTML = "Zamknij";
    div.appendChild(img);
    div.appendChild(button);

    button.addEventListener('click', onClickButtonHandler);
    
    document.body.appendChild(div);
}

function onClickButtonHandler() {
    var div = document.querySelector('.fullScreen');
    div.parentNode.removeChild(div);
}
