document.addEventListener("DOMContentLoaded", function () {
    // Poniżej napisz kod rozwiązujący zadania

    // zadanie 1

    var ex1 = document.querySelector(".ex1");
    var chrome = ex1.querySelector("div > .chrome");
    var aChrome = ex1.querySelector("div > a");
    var edge = ex1.querySelector("div > .edge");
    var aEdge = edge.nextElementSibling;
    var firefox = ex1.querySelector("div > .firefox");
    var aFirefox = firefox.nextElementSibling;


    aChrome.innerText = "Chrome";

    edge.style.backgroundImage = "url('https://mk0intowindows84fvao.kinstacdn.com/wp-content/uploads/2017/11/Mute-Edge-tabs-with-a-click-2_thumb.png')";

    aEdge.href = "https://www.microsoft.com/pl-pl/windows/microsoft-edge";

    firefox.style.backgroundImage = "url('https://cdn.vox-cdn.com/thumbor/5q3u8YnxPJ4-MTgfH-oAW3pROKE=/0x0:2040x1360/920x613/filters:focal(857x517:1183x843):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/59396599/ff12.0.jpg')";

    aFirefox.innerText = "Firefox";

    aFirefox.href = "https://www.mozilla.org/pl/firefox/new/?utm_source=google&utm_medium=cpc&utm_campaign=Firefox-Brand-PL-TS-GGL-Exact&utm_term=firefox&utm_content=A144_A203_A006327&gclid=CjwKCAjwyMfZBRAXEiwA-R3gMy4HP2j0FXEkBYxfiidhdVWPTVgTnvQHk_5hoTJIeKfuGYzwvHbyehoCDbwQAvD_BwE&gclsrc=aw.ds";

    chrome.style.width = "100px";

    // style CSS zostały przypisane inline, ponieważ style inline są najwyżej w hierarchii 

    //zadanie 2

    // var ex2 = document.querySelector(".ex2");
    // console.log(ex2);

    var spanName = document.querySelector("#name")
    console.log(spanName);
    var spanFavCol = document.querySelector("#fav_color");
    console.log(spanFavCol);
    var spanFavMeal = document.querySelector("#fav_meal");
    console.log(spanFavMeal);

    spanName.innerHTML = "Alicja Kłos";
    spanFavCol.innerHTML = "green";
    spanFavMeal.innerHTML = "vegeburger";

    // zadanie 3

    var ex3 = document.querySelector(".ex3");
    console.log(ex3);
    var ul = ex3.querySelector("ul");
    console.log(ul);
    var lis = ul.querySelectorAll("li");
    console.log(lis);

    ul.classList.add("menu");

    for (var i = 0; i < lis.length; i++) {

        lis[i].classList.add("menuElement");

    }

    for (var i = 0; i < lis.length; i++) {
        if (lis[i].classList.contains("error")) {
            lis[i].classList.remove("error")
        }
    }

    //zadanie 4

    var liEx4 = document.querySelectorAll(".ex4 li");


    var counter = 1;
    var counter2 = 1;

    for (var i = 0; i < liEx4.length; i++) {

        liEx4[i].dataset.id = counter;

        counter++;

    }


    // drugi sposob 

    // for (var i = 0; i < liEx4.length; i++) {

    //     liEx4[i].setAttribute("data-id", counter2)

    //     counter2++;

    // }

    console.log(liEx4);

});

