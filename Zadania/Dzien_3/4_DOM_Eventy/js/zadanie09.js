document.addEventListener("DOMContentLoaded", function() {
    var a = document.getElementById("a");
    var b = document.getElementById("b");
 
    console.log("obiekt document: ", this); //this to document
 
    a.addEventListener("click", function(event) { //tu this jest a
      console.log("a: ", this);
    });
 
    b.addEventListener("click", function(event) { //to this b small
      console.log("b: ", this);
 
      function innerFuncOne(event) {
          console.log("innerFuncOne: ", this);
          console.log('tu mamy naszego', event.target);
          event.target.style.backgroundColor = 'red';
      }
 
      innerFuncOne(event);
    });
});

