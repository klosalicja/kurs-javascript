document.addEventListener('DOMContentLoaded', function(){

    var div = document.querySelector('#box');
    var globalx = document.querySelector("#globalX");
    var globaly = document.querySelector("#globalY");
    var localx = document.querySelector("#localX");
    var localy = document.querySelector("#localY");

    div.addEventListener('mouseover', onMouseoverDivShowLocationHandler);

    function onMouseoverDivShowLocationHandler(event) {
  
        globalx.innerHTML = event.screenX;
        globaly.innerHTML = event.screenY;
        localx.innerHTML = event.clientX;
        localy.innerHTML = event.clientY;
    }

});