document.addEventListener('DOMContentLoaded', function() {

    var spanWidth = document.querySelector("#windowWidth");
    var spanHeight = document.querySelector("#windowHeight");

    spanWidth.innerHTML = innerWidth;
    spanHeight.innerHTML = innerHeight;

    window.addEventListener('resize', onResizeWindowChangeResizeHandler);

    function onResizeWindowChangeResizeHandler() {

        spanWidth.innerHTML = innerWidth;
        spanHeight.innerHTML = innerHeight;

    }
});