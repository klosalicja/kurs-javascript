document.addEventListener('DOMContentLoaded', function(){

    var counter = document.querySelector('.counter');
    var buttonElements = document.querySelectorAll('button');
    var clickCounter = 0;

    for (var i = 0; i < buttonElements.length; i++) {
        buttonElements[i].addEventListener("click", onClickButtonsAddCounterHandler);
    }

    function onClickButtonsAddCounterHandler() {

        clickCounter++;
        counter.innerHTML = clickCounter;
        
        }

});