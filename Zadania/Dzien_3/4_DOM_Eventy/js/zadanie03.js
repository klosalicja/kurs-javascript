document.addEventListener('DOMContentLoaded', function() {
    var buttons = document.querySelectorAll('button');

    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', onClickButtonsAddCounterHandler);
    } 

    function onClickButtonsAddCounterHandler() {
        var spanElement = this.nextElementSibling.firstElementChild;
        var counter = parseInt(spanElement.innerHTML);

        spanElement.innerHTML = ++counter;
    }

});