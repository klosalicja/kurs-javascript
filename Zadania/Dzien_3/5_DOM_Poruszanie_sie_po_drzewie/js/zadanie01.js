var buttons = document.querySelectorAll('.button');

for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', onClickButtonHiddenOrShowHandler);
}

function onClickButtonHiddenOrShowHandler() { 
    var nextElement = this.nextElementSibling;

    if (nextElement !== null) {
        nextElement.classList.toggle('hidden');
    }
}