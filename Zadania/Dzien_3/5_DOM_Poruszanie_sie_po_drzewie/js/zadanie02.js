document.addEventListener('DOMContentLoaded', function() {
    var buttons = document.querySelectorAll('.button');

    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', onClickButtonChangeColorHandler);
    }

    function onClickButtonChangeColorHandler() {
        var randomColor = "#" + Math.floor(Math.random()*16777215).toString(16);
        var parent = this.parentElement;

        parent.style.backgroundColor = randomColor;
    }




});