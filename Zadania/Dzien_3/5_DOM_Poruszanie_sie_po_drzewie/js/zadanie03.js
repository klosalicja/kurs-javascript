document.addEventListener('DOMContentLoaded', function() {
    var divs = document.querySelectorAll('.listContainer');
   
    for (var i = 0; i < divs.length; i++) {
        divs[i].addEventListener('mouseover', onClickDivschangeColorsHandler);
    }

    function onClickDivschangeColorsHandler() {

        var lis = this.firstElementChild.children;

        this.firstElementChild.firstElementChild.style.backgroundColor = 'red';
        this.firstElementChild.lastElementChild.style.backgroundColor = 'blue';
        
        for (var i = 1; i < lis.length -1; i++) {
            lis[i].style.backgroundColor = 'green';   
        }
    }
});