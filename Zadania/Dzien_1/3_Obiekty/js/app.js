// zadanie 1


var book = {
    title: "Słoń",
    author: "Kowalski",
    numberOfPages: 260
}

console.log(book);

//zadanie 2

var person = {
    name: "kasia", 
    age: 15,
    sayHello: function() {

        return "hello";

    } 
}
console.log(person);
console.log(person.sayHello());

//zadanie 3

var train = {
    
}

console.log(train instanceof Object);

//zadanie 4 


var car = {
    brand: "Honda",
    color: "Czerwona",
    numberOfKilometers: 0,
    printCarinfo: function() {
        return this.color + " " + this.brand +" " + this.numberOfKilometers;
    },
    drive: function(kilometers) {
        
        return this.numberOfKilometers += kilometers;
    }

}

console.log(car.printCarinfo());
console.log(car.drive(15));
console.log(car.printCarinfo());


//zadanie 5


car.array = ["11.02.1990", "13.06.2015", "12.09.2018"];

console.log(car);

car.newInfo = function(newDate) {
    this.array.push(newDate); 
}

car.returnArray = function() {
    return this.array;
}

console.log(car);
console.log(car.newInfo("12.03.2020"));
console.log(car.returnArray());

//zadanie 6 

var bird = {
    type: "papugowe",
    name: "Ara",
    getType: function() {
        return this.type;
    }
}

console.log(bird instanceof Object); //true, tak jak poprzednio


//zadanie 7

var myString = "Oko cyklonu dosięgnie Cię wszędzie!";

console.log(myString instanceof Object); //false
console.log(typeof myString); //string

var myNumber = 3;

console.log(myNumber instanceof Object);// false 
console.log(typeof myNumber); //number

//zadanie 8 

var Robot = function(name) {
    this.name = name;
    this.isFunctional = true;
}

Robot.prototype.sayHi = function(toWho) {
  if (this.isFunctional === true) {
    console.log('Robot ' + this.name + ' greets ' + toWho); // to jest parametr, a nie string
  } else {
    console.log('Robot ' + this.name + ' is broken');
  }
};

var newname = 'Robot3';

Robot.prototype.changeName = function(newname) {
    console.log('Robot ' + this.name + ' changes name to ' + newname);
};

Robot.prototype.fixIt = function() {
    this.isFunctional = true;

    console.log('Robot ' + this.name + ' was fixed');
};

var robot1 = new Robot("krzyś");
var robot2 = new Robot("zenek");
var robot3 = new Robot("kazik");
var robot4 = new Robot("mietek");

console.log(robot1);
console.log(robot2);
console.log(robot3);
console.log(robot4);

console.log(robot1.sayHi("ala"));
console.log(robot2.changeName("kasia"));
console.log(robot3.fixIt());
console.log(robot4.sayHi("ela"));

//zadanie 9 

var Rectangle = {
    width: 20,
    height: 20,
    getArea: function() {
        return this.width * this.height;
     },
     getPermiter:function() {
        return 2*(this.width + this.height);
    }
      
}

var rectangle2 = Object.create(Rectangle, {
    width: {value: 10},
    height: {value:20}
});

var rectangle3 = Object.create(Rectangle, {
    width: {value: 30},
    height: {value:30}
});

var rectangle4 = Object.create(Rectangle, {
    width: {value: 5},
    height: {value:20}
});

console.log(rectangle2);
console.log(rectangle3);
console.log(rectangle4);

console.log(rectangle2.getArea());
console.log(rectangle2.getPermiter());
console.log(rectangle2.hasOwnProperty('getArea'));
console.log(rectangle3.getArea());
console.log(rectangle3.getPermiter());
console.log(rectangle3.hasOwnProperty('getArea'));
console.log(rectangle3.hasOwnProperty('getPermiter'));
console.log(rectangle4.getArea());
console.log(rectangle4.getPermiter());
console.log(rectangle4.hasOwnProperty('getArea'));


//hasOwnProperty zwraca false, są one dostępne w prototypie.


//zadanie 10

var Calculator = function() {
    this.array = [];
}

Calculator.prototype.add = function(num1, num2) {
    var result = num1 + num2;
    var newEl = 'added ' + num1 + ' to ' + num2 + ' got ' + result;
    this.array.push(newEl);
    return result;
};

Calculator.prototype.multiply = function(num1, num2) {

    var result = num1 * num2;
    var newEl = "multiplied " + num1 + ' with ' + num2 + ' got ' + result;
    this.array.push(newEl);
    return result;
};
Calculator.prototype.substract = function(num1, num2) {

    var result = num1 - num2;
    var newEl = "substracted " + num1 + ' from ' + num2 + ' got ' + result;
    this.array.push(newEl);
    return result;
};
Calculator.prototype.divide = function(num1, num2) {

    var result = num1 / num2;
    var newEl = "divided " + num1 + ' by ' + num2 + ' got ' + result;
    this.array.push(newEl);
    return result;
};
Calculator.prototype.printOperations = function() {

    for(var i = 0; i<this.array.length; i++) {
       console.log(this.array[i]);
    }
    
};
Calculator.prototype.clearOperations = function() {

    this.array = [];
    
};

var calculator1 = new Calculator();

console.log(calculator1);

console.log(calculator1.add(1, 3));
console.log(calculator1.multiply(1, 3));
console.log(calculator1.substract(1, 3));
console.log(calculator1.divide(6, 3));
console.log(calculator1.printOperations());
console.log(calculator1.clearOperations());
console.log(calculator1);
