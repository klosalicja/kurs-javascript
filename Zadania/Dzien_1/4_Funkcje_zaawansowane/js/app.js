//zadanie 0 niechcący tez zrobiłam

//deklarujemy funkcję jeden
function jeden() {
  // zadeklarowana zmienna1 jest widoczna w funkcji jeden i w funkcji dwa
  var zmienna1 = 1;

  //deklarujemy funkcje w funkcji
  function dwa() {
    //Nie ma w funkcji dwa takiej zmiennej, więc szukamy dalej i znajdujemy ją w funkcji jeden.
    console.log(zmienna1);

    //deklarujemy zmienną - Ta zmienna będzie widoczna tylko tutaj.
    var zmienna2 = 3;
  }

  //wywołanie funckji dwa - można ją wywołać bez problemu, ponieważ funkcja dwa ma dostęp do potzrebnej jej zmienna1;
  dwa();

  //Zmienna2 nie jest tu widoczna, poniewaz ogranicza ją zakres funkcji dwa, zasięg działa na zew. ale nie do wew.
  // console.log(zmienna2);
}

//przy wywołaniu funkcji jeden powinniśmy najpierw wywołać funcję dwa i to się udaje, a potem wypisac zmienną 2 i to sie nie udaje, bo nie funkcja jeden nie ma do niej dostępu:)
jeden();

//zadanie 1

//deklarujemy funkcję

function sortArray() {
  //deklarujemy tablicę
  var points = [41, 3, 6, 1, 114, 54, 64];

  // metoda sort na tablicy ktora ma nam ją posegregować
  points.sort(function(a, b) {
    //w metodzie sort przekazujemy funkcję callback, jeśli zwroci nam 1 to znaczy, że a > b, czyli zamiana, jesli 0 to zostają tam gdzie są, bo sa rowne, jest -1 to znaczy, że a<b, czyli nie ma zamiany
    return a - b;
  });

  //zwraca posortowaną tablicę od najmiejszej liczby do największej
  return points;
}

//wywołanie funkcji sortArray
console.log(sortArray());

//zadanie 2


var showMaxNumber = function() {
  var max = arguments[0];

  for (var i = 0; i < arguments.length; i++) {
    if (arguments[i] > max) {
      max = arguments[i];
    }
  }
  return max;
};

console.log(showMaxNumber(1, 2, 4, 5, 7, 3, 45));

//zadanie 3

var countHello = function(nr) {
  var counter = 0;

  if (nr < 1 || nr > 10) {
    return "Podaj nr z zakresu od 1 do 10";
  }
  else {

  var time = setInterval(function() {
    console.log("hello " + counter);
    counter++;

    if (counter == nr) {
      clearInterval(time);
    }
  }, 1000);
 }
}
console.log(countHello(5));
console.log(countHello(11));

//zadanie 4

hello();
// hi()

function hello() {
  console.log("Witaj");
}

var hi = function() {
  console.log("CZeść");
};

hi();
hello();
//jeśli zadeklarujemy funkję jako wyrażenie funkcyjne to możemy ją wywolać dopiero po zdefiniowaniu, a jesli poprzez słowo kluczowe funcion to gdzie chcemy wywołujemy, bo jest zjawisko hoistingu.
