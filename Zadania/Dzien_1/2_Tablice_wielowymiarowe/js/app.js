var task1Array = [
  [2, 3],
  ["Ala", "Ola"],
  [true, false],
  [5, 6, 7, 8],
  [12, 15, 67]
];

var task2Array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]];

//Zadanie 1

console.log(task1Array[3][2]);
console.log(task1Array[1].length);
console.log(task1Array[4][2]);

//zadanie 2

var task2Array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]];

//podpunkt 1.

for (var i = 0; i < task2Array.length; i++) {
  console.log(task2Array[i]);
}

//podpunkt 2.

for (var i = 0; i < task2Array.length; i++) {
  console.log(task2Array[i].length);
}

// podpunkt 3.

for (var i = 0; i < task2Array.length; i++) {
  for (var j = 0; j < task2Array[i].length; j++) {
    console.log(task2Array[i][j]);
  }
}

//zadanie 3

// function print2DArray(array) {
//   for (var i = 0; i < array.length; i++) {
//     console.log(array[i]);
//   }
// }

// var task2Array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]];

// console.log(print2DArray(task2Array));

// wszystkie elementy

function print2DArray(array) {
  for (var i = 0; i < array.length; i++) {
    for(var j = 0; j<array[i].length; j++) {
      console.log(array[i][j]);
    }
  }
}

var task2Array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]];

console.log(print2DArray(task2Array));

//zadanie 4

var task3Array = [
  ["ala", "ola", "kasia"],
  ["alek", "olek", "kazik"],
  ["kot", "pies", "kaczka", "dzik"]
];

console.log(print2DArray(task3Array));

//zadanie 5

function create2DArray(rows, columns) {
  var newArray = [];

  var counter = 0;

  for (var i = 0; i < rows; i++) {
    newArray[i] = [];

    for (var j = 0; j < columns; j++) {
      newArray[i][j] = counter;
      counter++;
    }
  }

  return newArray;
}

console.log(create2DArray(4, 4));
