//zadanie 1

var fruitsTab = ["malina", "truskawka", "granat", "jeżyna"];

console.log(fruitsTab[0]);
console.log(fruitsTab[fruitsTab.length - 1]);

for (var i = 0; i < fruitsTab.length; i++) {
  console.log(fruitsTab[i]);
}

//zadanie 2

function createArray(number) {
  var newArray = [];

  for (var counter = 1; counter <= number; counter++) {
    newArray.push(counter);
  }

  return newArray;
}

console.log("tablica z liczbami do 6 = " + createArray(6));
console.log("tablica z liczbami do 1 = " + createArray(1));
console.log("Test dla liczby ujemnej (powinna być pusta tablica) " + createArray(-6));
console.log("Test dla zera (powinna być pusta tablica) " + createArray(0));

//zadanie 3

function printTable(bufor) {

  for (var i = 0; i < bufor.length; i++) {
    console.log(bufor[i]);

  }
}

var array = ["ozi", "loki", "miko"];

console.log(printTable(array));

//zadanie 4

function multiply(array) {

  var result = 1;

  for (var i = 0; i < array.length; i++) {
    result *= array[i];
  }
  return result;
}

console.log(multiply([1, 2, 3, 4, 5, 6, 7]));
console.log(multiply([1, 1, 1, 1]));
console.log(multiply([2, 8, 3, 7]));

//zadanie 5

function getEvenAvarage(array) {

  var result = 0;
  var counter = 0;

  for (var i = 0; i < array.length; i++) {
    if (array[i] % 2 === 0) {
      result += array[i];
      counter++;
    }
  }
  if (counter === 0) {
    return null;
  } else {
    return result / counter;
  }
}

console.log(getEvenAvarage([1, 2, 3, 4, 5, 6, 7]));
console.log(getEvenAvarage([1, 1, 1, 1]));
console.log(getEvenAvarage([2, 8, 3, 7, 4]));

//zadanie 6

function sortArray(array) {

  var newArray = [];
  newArray = array.sort(function (a, b) {
    return a - b;
  });
  return newArray;
}

function sortArray2(array) {
  var change;
  var bufor;

  do {
    change = false;
    for (var i = 0; i < array.length - 1; i++) {
      if (array[i + 1] < array[i]) {
        bufor = array[i];
        array[i] = array[i + 1];
        array[i + 1] = bufor;
        change = true;
      }
    }
  }
  while (change);
  return array;

}
console.log(sortArray2([145, 11, 3, 64, 4, 6, 10]));

function sortArray3(array) {

  var bufor;


    for (var i = 0; i < array.length - 1; i++) {
      for( var j = i+1;j<array.length;j++) {
        if (array[i] > array[j]) {
          bufor = array[i];
          array[i] = array[j];
          array[j] = bufor;
        }
      }
    }
      
 
  return array;

}
console.log(sortArray3([145, 11, 3, 64, 4, 6, 10]));


//zadanie 7 - ?

function addArrays(array, array2) {

  var newArray = [];
  var connetedArray = [];
  var result = 0;
  var subtrationL = array.length - array2.length;
  var newArrayShort = [];
  var newArrayShort2 = [];


  if (subtrationL === -1 || subtrationL === 0) {

    for (var i = 0; i < array.length; i++) {

      result = array[i] += array2[i];
      newArray.push(result);

    }

    newArrayShort = array2.splice(array.length);
    connetedArray = newArray.concat(newArrayShort);

  } else {

    for (var i = 0; i < array2.length; i++) {

      result = array2[i] += array[i];
      newArray.push(result);

    }

    newArrayShort2 = array.splice(array2.length);
    connetedArray = newArray.concat(newArrayShort2);

  }

  return connetedArray;

}

console.log(addArrays([4, 0, 1, 3, 4], [1, 9, 6, 7, 8, 17]));
console.log(addArrays([8, 3, 22], [1, 3, 2]));
console.log(addArrays([2, 3, 1, 5, 3, 5], [3, 1, 76, 1]));


function addArrays1(array, array2) {

  var subtrationL = array.length - array2.length;
  var rest = array2.length - array.length;
  var newArray = [];

  if (subtrationL === -1) {

    for (var i = 0; i < array.length; i++) {

      array[i] += array2[i];

    }

    newArray = array2.slice(subtrationL);
    return array.concat(newArray); // czy ma tylko zwrocić wynik czy ma zwrocic nową tablice?

  } else if (subtrationL === 0) {

    for (var i = 0; i < array.length; i++) {

      array[i] += array2[i];

    }
    return array;
  } else {

    for (var i = 0; i < array2.length; i++) {

      array2[i] += array[i];

    }

    newArray = array.slice(rest);
    return array2.concat(newArray);
  }

}

console.log(addArrays1([4, 0, 1, 3, 4], [1, 9, 6, 7, 8, 17]));
console.log(addArrays1([8, 3, 22], [1, 3, 2]));
console.log(addArrays1([2, 3, 1, 5, 3, 5], [3, 1, 76, 1]));



function addArrays2(array, array2) {

  var subtrationL = array.length - array2.length;
  var newArray = [];
  var counter = 0;
 
  if (subtrationL === -1) {
     
      while (array2.length !== array.length) {
        array.push(0);
        counter++;
      }

      for (var i = 0; i < array2.length; i++) {

      newArray.push(array[i] += array2[i]);

    }

    return newArray;

  }

  else if(subtrationL === 0) {

     for (var i = 0; i < array.length; i++) {

     newArray.push(array[i] += array2[i]);

    }
    return newArray;
  }

  else {

    while (array.length !== array2.length) {
      array2.push(0);
      counter++;
    }

    for (var i = 0; i < array.length; i++) {

      newArray.push(array2[i] += array[i]);

    }

    return newArray;
  }

}

console.log(addArrays2([4, 0, 1, 3, 4], [1, 9, 6, 7, 8, 17]));
console.log(addArrays2([8, 3, 22], [1, 3, 2]));
console.log(addArrays2([2, 3, 1, 5, 3, 5], [3, 1, 76, 1]));



