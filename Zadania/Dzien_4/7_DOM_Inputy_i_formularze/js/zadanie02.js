document.addEventListener('DOMContentLoaded', function() {
    var imgElements = document.querySelectorAll('img');
    var form = document.querySelector('form');
    var select = form.querySelector('.form-control');

    for (var i = 0; i < imgElements.length; i++) {
        imgElements[i].style.display = 'none';
    } 
 
    form.addEventListener('submit', onChangeButtonShowImageHandler);

    function onChangeButtonShowImageHandler(event) {
        event.preventDefault();
        var selectOption = select.options[select.selectedIndex].innerHTML;

        for (var i = 0; i < imgElements.length; i++) {

            if (selectOption == imgElements[i].alt) {    
                imgElements[i].style.display = 'block';         
            } else if (selectOption == 'Os X' && imgElements[i].alt == 'Apple') {
                imgElements[i].style.display = 'block';
            } else {
                imgElements[i].style.display = 'none';
            }
        }
    }
});


