document.addEventListener('DOMContentLoaded', function() {
    var form = document.querySelector('form');
    var name = form.elements.name; 
    console.log(form.elements);
    
    name.addEventListener('keyup', onKeyupFieldshandler);   
    
    function onKeyupFieldshandler(event) {  
        
        var field = event.currentTarget;
        var groupAddon = field.nextElementSibling;

            console.log(field.length);
            groupAddon.innerHTML = '';

            switch(field.value[0]) {
                case '4':
                visaValidator3(field, groupAddon);
                (!visaValidator2(field) && checkLengthValidator(field)) && addMark(groupAddon);
                break;
                case '5':
                mastercardValidator(field, groupAddon);
                (!mastercardValidator2(field) && checkLengthValidator(field)) && addMark(groupAddon);
                break;
                case '3':
                expressValidator(field) && expressValidator2(field, groupAddon);
                (!expressValidator3(field) && checkLengthValidator(field)) && addMark(groupAddon);
                break;
                default:
                addMark(groupAddon);
            }
    }
       
        function  visaValidator2(field) {
            return field.value.length >= 13 && field.value.length <= 16;
        }

        function visaValidator3(field, groupAddon) {
            groupAddon.innerHTML = 'Visa';
            visaValidator2(field) && addCheck(groupAddon);
        }

        function mastercardValidator(field, groupAddon) {
            groupAddon.innerHTML = 'Mastercard';
            mastercardValidator2(field) && addCheck(groupAddon);
        }

        function mastercardValidator2(field) {
            return field.value.length == 16;
        }

        function expressValidator(field) {
            return field.value[1] == '4' || field.value[1] == '7';
        }

        function expressValidator2(field, groupAddon) {
            groupAddon.innerHTML = 'American Express';
            expressValidator3(field) && addCheck(groupAddon);
        }

        function expressValidator3(field) {
            return field.value.length == 15;
        }

        function checkLengthValidator(field) {
            return field.value.length > 15;
        }

        function addMark(groupAddon) {
            groupAddon.innerHTML = '?';
        }

        function addCheck(groupAddon) {
            groupAddon.innerHTML += '<i class="fas fa-check"></i>';
        }
    
})

// if(name.value[0] == '4') {
//     groupAddon.innerHTML = 'Visa'; // tu jest ok

//     if(name.value.length >= 13 && name.value.length <= 16) {
//         groupAddon.innerHTML += '<i class="fas fa-check"></i>'; 
//     }
//     else if (name.value.length > 16) {
//         groupAddon.innerHTML = '?';
//     }
// }
// if(name.value[0] == '5') {
//     groupAddon.innerHTML = 'Mastercard'; // tu jest ok

//     if(name.value.length == 16) {
//         groupAddon.innerHTML += '<i class="fas fa-check"></i>'; 
//     }
//     else if (name.value.length >16) {
//         groupAddon.innerHTML = '?';
//     }
// }
// if(name.value[0] == '3' && name.value[1] == '4' || name.value[1] == '7') {
//     groupAddon.innerHTML = 'American Express'; // tu jest ok

//     if(name.value.length == '15') {
//         groupAddon.innerHTML += '<i class="fas fa-check"></i>'; 
//     }
//     else if (name.value.length > 15) {
//         groupAddon.innerHTML = '?';
//     }
// }