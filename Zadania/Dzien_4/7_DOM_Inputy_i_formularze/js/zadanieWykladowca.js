document.addEventListener('DOMContentLoaded', function() {

    var form = document.querySelector('form');
    var fields = form.elements;
    var errorMessage = document.querySelector('.error-message');
    
    form.addEventListener('submit', function(event) {
        var errors = [];

        for (var i = 0; i < this.elements.length; i++ ) {
            var field = this.elements[i];
            switch(field.name) {
                case 'email':
                    !emailValidator(field) && errors.push('Email musi posiadac znak @');
                    break;
                case 'name':
                    !firstNameValidator(field) && errors.push('Imie musi miec min. 6 znakow');
                    break;
                case 'surname':
                    !surnameValidator(field) && errors.push('Nazwisko musi miec min. 6 znakow');
                    break;
                case 'pass1':
                    !passwordValidator(field) && errors.push('Hasła muszą byc identyczne');
                    break;
                case 'agree':
                    !checkboxValidator(field) && errors.push('Musisz zaakceptować warunki');
                    break;
            }
        } //tu się konczy petla więc enter

        if (errors.length) {
            event.preventDefault();
            console.log(errors);
            showErrors(errors);
        }
        
    });

    //funkcje ponizej wszystkie!!!!
    function emailValidator(field) {
        return field.value.indexOf('@') > -1;
    }

    function minSixLenghtValidator(field) {
        return field.value.length >= 6;
    }

    function firstNameValidator(field) {
        return minSixLenghtValidator(field);
    }

    function surnameValidator(field) {
        return minSixLenghtValidator(field);
    }

    function passwordValidator(field) {
        return field.value === fields.pass2.value;
    }

    function checkboxValidator(field) {
        return field.checked;
    }

    function showErrors(errors) {
        errorMessage.innerHTML = ''; //zawsze czyscimy
        var ulElement = document.createElement('ul');
        
        for (var it = 0; it < errors.length; it++) {
            var li = document.createElement('li');
            li.innerHTML = errors[it];
            ulElement.appendChild(li);
        }

        errorMessage.appendChild(ulElement);
    }

});