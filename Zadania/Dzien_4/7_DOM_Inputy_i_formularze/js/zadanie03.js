document.addEventListener('DOMContentLoaded', function() {
    var form = document.querySelector('form');
    var fields = form.elements;
    var errorMessage = document.querySelector('.error-message');

    form.addEventListener('submit', onSubmitValidationHandler);
    
    function onSubmitValidationHandler(event) { 
        event.preventDefault();   
        var errors = [];

        for (var i = 0; i < this.elements.length; i++) {
            var field = this.elements[i];
            switch(field.name) {
                case 'email':
                !teamsValidator(field) && errors.push('Drużyny muszą być rożne');
                break;
                case 'surname':
                !pointsValidator(field) && errors.push('Liczba punktow nie może być ujemna');
                break;
            }
        }

        if (errors.length) {
            event.preventDefault();
            console.log(errors);
            showErrors(errors);
        }
        
        if(errors.length == 0) {
            addScore2();
        }
    }

    function teamsValidator(field) {
        return field.value !== fields.name.value;
    }

    function pointsValidator(field) {
        return field.value > -1;
    }

    function showErrors(errors) {
        errorMessage.innerHTML = ''; 
        var ulElement = document.createElement('ul');
        
        for (var it = 0; it < errors.length; it++) {
            var li = document.createElement('li');
            li.innerHTML = errors[it];
            ulElement.appendChild(li);
        }

        errorMessage.appendChild(ulElement);
    }

    //tu tworzę poprzez klonowanie, teraz jest ok :) 

    function addScore() {
        var table = document.querySelector('table.table.table-bordered');
        var tbody = table.querySelector('tbody');
        var trElement = table.firstElementChild.lastElementChild;
        var copyElements = trElement.cloneNode(true);

        copyElements.firstElementChild.innerHTML = fields.email.value;
        copyElements.firstElementChild.nextElementSibling.innerHTML =  fields.name.value;
        copyElements.lastElementChild.innerHTML = fields.points1.value + ' - ' + fields.points2.value;
        tbody.appendChild(copyElements);
    }

    // tu poprzez createElement ten sposob lepszy

    function addScore2() {
        var table = document.querySelector('table.table.table-bordered');
        var tbody = document.createElement('tbody')
        var trElement = document.createElement('tr');
        
        for (var i = 0; i < 3; i++) {           
            var tdElement = document.createElement('td');

            trElement.appendChild(tdElement);
        }
        
        trElement.firstElementChild.innerHTML = fields.email.value;
        trElement.firstElementChild.nextElementSibling.innerHTML = fields.name.value;
        trElement.lastElementChild.innerHTML = fields.points1.value + ' - ' + fields.points2.value;
        tbody.appendChild(trElement);
        table.appendChild(tbody);  
    }
});