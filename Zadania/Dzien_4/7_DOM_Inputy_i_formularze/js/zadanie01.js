document.addEventListener('DOMContentLoaded', function() {

   var invoiceData = document.querySelector('#invoiceData');
   var checkboxElement = document.querySelector('#invoice');

   invoiceData.style.display = 'none';
   
   checkboxElement.addEventListener('change', onChangeInputHiddenHandler);

   function onChangeInputHiddenHandler() {
        var hidden = this.parentElement.parentElement.nextElementSibling;
    
        hidden.style.display = 'none';

        if (this.checked) {
            
            hidden.style.display = 'block';
        }
    }
});