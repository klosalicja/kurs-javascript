document.addEventListener('DOMContentLoaded', function() {

    var buttonAdd = document.querySelector("#addBtn");

    buttonAdd.addEventListener('click', onClickButtonAddOrderHandler);

    function onClickButtonAddOrderHandler() {
        var table = document.querySelector('table');
        var trElement = table.querySelector('tr');
        var copyElements = trElement.cloneNode(true);
        var inputId = document.querySelector('#orderId');
        var inputItem = document.querySelector('#item');
        var inputQuantity = document.querySelector('#quantity');
      
        copyElements.firstElementChild.innerHTML = inputId.value;
        copyElements.firstElementChild.nextElementSibling.innerHTML = inputItem.value;
        copyElements.lastElementChild.innerHTML = inputQuantity.value;
        table.appendChild(copyElements);
    }

});