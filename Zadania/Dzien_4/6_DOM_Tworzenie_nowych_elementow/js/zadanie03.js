document.addEventListener('DOMContentLoaded', function() {

    var button = document.querySelector('.button');

    button.addEventListener('click', onClickButtonRemoveHandler);

    function onClickButtonRemoveHandler(e) {

        var parentN = this.parentNode;

        parentN.remove(e.currentTarget);
        // lub parentN.innerHTML = "";
    }

});