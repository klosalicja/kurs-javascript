document.addEventListener('DOMContentLoaded', function() {
    var button = document.querySelector('.button');

    button.addEventListener('click', onClickButtonAddLiHandler);

    function onClickButtonAddLiHandler() {
        var ul = this.previousElementSibling;
        var newLi = document.createElement('li');
        var lis = ul.querySelectorAll('li');
        
        newLi.innerHTML = "W chwili dodania było " + lis.length + " elementow";
        ul.appendChild(newLi);
    }
});