document.addEventListener('DOMContentLoaded', function() {

    var button = document.querySelector('.button');

    button.addEventListener('click', onClickButtonRemoveListHandler)

    function onClickButtonRemoveListHandler() {
        var ulList = this.previousElementSibling;

        ulList.remove(ulList.children);
    }

});