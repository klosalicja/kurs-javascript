document.addEventListener('DOMContentLoaded', function() {

    var buttons = document.querySelectorAll('.moveBtn');

    for(var i = 0; i<buttons.length; i++) {
        buttons[i].addEventListener('click', onClickButtonChangePlaceHandler);
    }

    function onClickButtonChangePlaceHandler() {
        var list1 = document.querySelector('#list1');
        var list2 = document.querySelector('#list2');
        var change = this.parentElement;
        var parentChange = change.parentElement;

        if (parentChange == list1) {
            list2.appendChild(change);
        }
        if (parentChange == list2) {
            list1.appendChild(change);
        }
    }
});